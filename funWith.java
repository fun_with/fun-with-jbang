///usr/bin/env jbang "$0" "$@" ; exit $?
//DEPS info.picocli:picocli:4.5.0

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.Callable;

import static java.lang.System.out;
import static java.util.Arrays.asList;

@Command(name = "funWith", mixinStandardHelpOptions = true, version = "funWith 0.1",
        description = "funWith made with jbang")
class funWith implements Callable<Integer> {

    @Parameters(index = "0", description = "The greeting to print", defaultValue = "World!")
    private String greeting;

    public static void main(String... args) throws IOException {
        listGitDirs("/Users/mvincent/work/gitlab");
        int exitCode = new CommandLine(new funWith()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception { // your business logic goes here...
        out.println("Hello " + greeting);
        return 0;
    }

    public static void listGitDirs(String dir) throws IOException {
        Files.list(Path.of(dir)).forEach(
                d -> {
                    if (d.toFile().isDirectory())
                        if (asList(d.toFile().list()).contains(".git")) {
                            out.println(d + " is a git project");
                        } else {
                            try {
                                listGitDirs(d.toRealPath(LinkOption.NOFOLLOW_LINKS).toString());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                }
        );
    }
}
